package co.istad.istadvdo;

import co.istad.istadvdo.api.article.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class IstadvdoApplication implements CommandLineRunner {

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private TopicRepository topicRepository;

	@Autowired
	private ArticleRepository articleRepository;

	public static void main(String[] args) {
		SpringApplication.run(IstadvdoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Topic topic1 = new Topic(1L, "Spring Boot", "Spring Boot", true);
		Topic topic2 = new Topic(2L, "PostgreSQL", "PostgreSQL", true);
		Topic topic3 = new Topic(3L, "ReactJS", "ReactJS", true);
		topicRepository.save(topic1);
		topicRepository.save(topic2);
		topicRepository.save(topic3);

		Tag tag1 = new Tag(1L, "HTML");
		Tag tag2 = new Tag(2L, "CSS");
		Tag tag3 = new Tag(3L, "JavaScript");
		Tag tag4 = new Tag(4L, "Thymeleaf");
		Tag tag5 = new Tag(5L, "JPA");
		tagRepository.save(tag1);
		tagRepository.save(tag2);
		tagRepository.save(tag3);
		tagRepository.save(tag4);
		tagRepository.save(tag5);

		Article article = new Article();
		article.setTitle("Learn JPA with Spring Boot");
		article.setDescription("Hibernate JPA");
		article.setStatus(true);
		article.setTopic(topic1);
		article.setTags(List.of(tag4, tag5));

		articleRepository.save(article);
	}
}
