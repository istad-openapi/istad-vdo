package co.istad.istadvdo.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TemplateController {

    @GetMapping("/html")
    String viewHtml() {
        return "mail-verification";
    }

}
