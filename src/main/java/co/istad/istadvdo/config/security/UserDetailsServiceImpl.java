package co.istad.istadvdo.config.security;

import co.istad.istadvdo.api.user.User;
import co.istad.istadvdo.api.user.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {

        User user = userMapper.selectByUsernameOrEmail(usernameOrEmail).orElseThrow(() ->
                new UsernameNotFoundException("User is not found..!"));

        CustomUserDetails customUserDetails = new CustomUserDetails();
        customUserDetails.setUser(user);

        log.info("CustomUserDetails = {}", customUserDetails);

        return customUserDetails;
    }
}
