package co.istad.istadvdo.api.file;

import co.istad.istadvdo.api.file.web.FileDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {

    FileDto uploadOne(MultipartFile file);

    List<FileDto> uploadAll(List<MultipartFile> files);

}
