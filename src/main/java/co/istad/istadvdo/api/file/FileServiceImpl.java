package co.istad.istadvdo.api.file;

import co.istad.istadvdo.api.file.web.FileDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final FileUtils fileUtils;

    @Override
    public FileDto uploadOne(MultipartFile file) {
        return fileUtils.save(file);
    }

    @Override
    public List<FileDto> uploadAll(List<MultipartFile> files) {

        List<FileDto> fileDtoList = new ArrayList<>();

        for (MultipartFile file : files) {
            fileDtoList.add(fileUtils.save(file));
        }

        return fileDtoList;
    }
}
