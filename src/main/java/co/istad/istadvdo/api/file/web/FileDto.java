package co.istad.istadvdo.api.file.web;

public record FileDto(
        String name,
        String uri,
        String extension,
        Long size
) {
}
