package co.istad.istadvdo.api.article.web;

import co.istad.istadvdo.api.article.TopicRepository;
import co.istad.istadvdo.api.base.Rest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;

@RestController
@RequestMapping("api/v1/topics")
@RequiredArgsConstructor
public class TopicRestController {

    private final TopicRepository topicRepository;
    private final Timestamp timestamp;

    @GetMapping
    Rest<?> findAllTopics() {

        return Rest.builder()
                .status(true)
                .code(HttpStatus.OK.value())
                .timestamp(timestamp)
                .message("Topics have been found!")
                .data(topicRepository.findAll())
                .build();
    }

}
