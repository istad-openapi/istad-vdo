package co.istad.istadvdo.api.article.web;

import co.istad.istadvdo.api.article.Article;
import co.istad.istadvdo.api.article.ArticleService;
import co.istad.istadvdo.api.article.ArticleServiceImpl;
import co.istad.istadvdo.api.base.Rest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@RestController
@RequestMapping("api/v1/articles")
@RequiredArgsConstructor
public class ArticleRestController {

    private final ArticleServiceImpl articleService;
    private final Timestamp timestamp;

    @PostMapping
    Rest<?> saveArticle(@RequestBody ArticleRequest articleRequest) {
        ArticleDto articleDto = articleService.saveArticle(articleRequest);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.CREATED.value())
                .timestamp(timestamp)
                .message("Article has been created!")
                .data(articleDto)
                .build();
    }

    @GetMapping
    Rest<?> findAllArticles(@RequestParam(required = false, defaultValue = "") String title) {

        Page<ArticleDto> articles = articleService.findAllArticles(title);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.OK.value())
                .timestamp(timestamp)
                .message("Articles have been found!")
                .data(articles)
                .build();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}")
    void deleteArticleById(@PathVariable Long id) {

        articleService.deleteArticleById(id);

    }

    @PatchMapping("{id}")
    Rest<?> updateArticleById(@PathVariable Long id, @RequestBody ArticleRequest articleRequest) {

        ArticleDto articleDto = articleService.updateArticleById(id, articleRequest);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.OK.value())
                .timestamp(timestamp)
                .message("Article has been updated!")
                .data(articleDto)
                .build();
    }

    @PutMapping("{id}")
    void updateArticleStatusById(@PathVariable Long id, @RequestParam Boolean status) {
        articleService.updateArticleStatusById(id, status);
    }

}
