package co.istad.istadvdo.api.article;

import co.istad.istadvdo.api.article.web.ArticleDto;
import co.istad.istadvdo.api.article.web.ArticleRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ArticleService {
    ArticleDto saveArticle(ArticleRequest articleRequest);

    Page<ArticleDto> findAllArticles(String title);

    void deleteArticleById(Long id);

    ArticleDto updateArticleById(Long id, ArticleRequest articleRequest);

    void updateArticleStatusById(Long id, Boolean status);
}
