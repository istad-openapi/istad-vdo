package co.istad.istadvdo.api.article;

import co.istad.istadvdo.api.article.web.ArticleDto;
import co.istad.istadvdo.api.article.web.ArticleRequest;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.data.domain.Page;

import java.util.stream.Stream;

@Mapper(componentModel = "spring")
public interface ArticleMapStruct {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Article fromArticleRequest(ArticleRequest articleRequest, @MappingTarget Article article);

    Article fromArticleRequestNew(ArticleRequest articleRequest);

    Article fromArticleDto(ArticleDto articleDto, @MappingTarget Article article);

    ArticleDto articleRequestToArticleDto(ArticleRequest articleRequest);

    ArticleDto toArticleDto(Article article);

    Iterable<ArticleDto> toIterableArticleDto(Iterable<Article> articles);

    Stream<ArticleDto> toPageArticleDto(Stream<Article> articlePage);

}
