package co.istad.istadvdo.api.article;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article, Long>,
        PagingAndSortingRepository<Article, Long> {

    Page<Article> findByTitleContainingIgnoreCase(String title, Pageable pageable);
    List<Article> findByTitleContainingIgnoreCaseAndStatusTrue(String title);


    @Query("SELECT a from Article a where a.status = :status")
    Article findArticleByStatus(Boolean status);

    @Modifying
    @Query("UPDATE Article a SET a.status = ?2 WHERE a.id = ?1")
    void updateStatusById(Long id, Boolean status);

}
