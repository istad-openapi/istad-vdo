package co.istad.istadvdo.api.article.web;

import java.util.List;

public record ArticleRequest(String title,
                             String description,
                             Long topicId,
                             List<Long> tagIds) {
}
