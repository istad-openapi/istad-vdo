package co.istad.istadvdo.api.article.web;

import co.istad.istadvdo.api.article.Tag;

import java.util.List;

public record ArticleDto(String title,
                         String description,
                         TopicDto topic,
                         List<TagDto> tags) {
}
