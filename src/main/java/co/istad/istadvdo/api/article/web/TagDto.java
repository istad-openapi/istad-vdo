package co.istad.istadvdo.api.article.web;

import java.util.List;

public record TagDto(String name,
                     String description) {
}
