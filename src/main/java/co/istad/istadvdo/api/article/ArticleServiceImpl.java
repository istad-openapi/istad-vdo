package co.istad.istadvdo.api.article;

import co.istad.istadvdo.api.article.web.ArticleDto;
import co.istad.istadvdo.api.article.web.ArticleRequest;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class ArticleServiceImpl implements ArticleService {
    private final ArticleRepository articleRepository;
    private final ArticleMapStruct articleMapStruct;

    @Override
    public ArticleDto saveArticle(ArticleRequest articleRequest) {

        Article article = articleMapStruct.fromArticleRequestNew(articleRequest);

        // Set up topic
        Topic topic = new Topic();
        topic.setId(articleRequest.topicId());
        article.setTopic(topic);

        // Set up tags
        List<Tag> tags = new ArrayList<>();
        articleRequest.tagIds().forEach((id) -> {
            Tag tag = new Tag();
            tag.setId(id);
            tags.add(tag);
        });
        article.setTags(tags);

        article.setStatus(true);

        articleRepository.save(article);

        article = articleRepository.findById(article.getId()).orElseThrow();

        return articleMapStruct.toArticleDto(article);
    }

    @Override
    public Page<ArticleDto> findAllArticles(String title) {

        Page<Article> articles = null;

        Pageable pageable = PageRequest.of(0, 2);

        if (!title.isEmpty()) {
            articles = articleRepository.findByTitleContainingIgnoreCase(title, pageable);
        } else {
            articles = articleRepository.findAll(pageable);
        }

        return articles
                .map(articleMapStruct::toArticleDto);
    }

    @Override
    public void deleteArticleById(Long id) {
        articleRepository.deleteById(id);
    }


    @Override
    public ArticleDto updateArticleById(Long id, ArticleRequest articleRequest) {

        Article article = articleRepository.findById(id).orElseThrow();

        System.out.println(article);

        /*Article newArticle = articleMapStruct.fromArticleRequest(articleRequest);

        if (articleRequest.topicId() != null) {
            // Set up topic
            Topic topic = new Topic();
            topic.setId(articleRequest.topicId());
            newArticle.setTopic(topic);
        }

        if (articleRequest.tagIds().size() > 0) {
            // Set up tags
            List<Tag> tags = new ArrayList<>();
            articleRequest.tagIds().forEach((tagId) -> {
                Tag tag = new Tag();
                tag.setId(tagId);
                tags.add(tag);
            });
            newArticle.setTags(tags);
        }


        System.out.println(article);
        newArticle.setId(id);
        newArticle.setStatus(article.getStatus());*/

        article = articleMapStruct.fromArticleRequest(articleRequest, article);
        System.out.println(article);

        articleRepository.save(article);

        return articleMapStruct.toArticleDto(article);
    }


    @Override
    public void updateArticleStatusById(Long id, Boolean status) {
        articleRepository.updateStatusById(id, status);
    }
}
