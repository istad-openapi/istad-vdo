package co.istad.istadvdo.api.article.web;

import java.util.List;

public record TopicDto(String name,
                       String description) {
}
