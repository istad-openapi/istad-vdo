package co.istad.istadvdo.api.playlist;

import co.istad.istadvdo.api.playlist.web.PlaylistDto;
import co.istad.istadvdo.api.playlist.web.PlaylistRequest;
import com.github.pagehelper.PageInfo;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PlaylistMapStruct {

    PageInfo<PlaylistDto> toPlaylistDtoPageInfo(PageInfo<Playlist> playlistPageInfo);

    Playlist fromPlaylistRequest(PlaylistRequest playlistRequest);

    PlaylistDto toPlaylistDto(Playlist playlist);

}
