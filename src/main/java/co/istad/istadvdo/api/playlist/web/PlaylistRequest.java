package co.istad.istadvdo.api.playlist.web;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record PlaylistRequest(
        @NotBlank(message = "Title is required..!")
        String title,
        @NotNull(message = "Playlist owner ID is required..!")
        Long ownerId,
        @NotBlank(message = "Visibility is required..!")
        String visibility,
        @NotBlank(message = "Thumbnail is required..!")
        String thumbnail
) {
}
