package co.istad.istadvdo.api.playlist;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Playlist {

    private Long id;
    private String uuid;
    private String title;
    private Long ownerId;
    private String visibility;
    private String thumbnail;
    private String accessCode;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private Boolean status;
}
