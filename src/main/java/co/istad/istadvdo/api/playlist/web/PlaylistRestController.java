package co.istad.istadvdo.api.playlist.web;

import co.istad.istadvdo.api.base.Rest;
import co.istad.istadvdo.api.playlist.PlaylistServiceImpl;
import com.github.pagehelper.PageInfo;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/playlists")
@RequiredArgsConstructor
public class PlaylistRestController {

    private final PlaylistServiceImpl playlistService;
    private final Timestamp timestamp;

    @GetMapping("{uuid}")
    Rest<?> findPlaylistByUuid(@PathVariable String uuid) {

        var playlistDto = playlistService.findPlaylistByUuid(uuid);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.OK.value())
                .timestamp(timestamp)
                .message("Playlist has been found!")
                .data(playlistDto)
                .build();
    }

    @GetMapping
    Rest<?> findPlaylistWithPaging(@RequestParam(defaultValue = "1", required = false) int pageNum,
                                   @RequestParam(defaultValue = "25", required = false) int pageSize) {

        var playlistDtoPageInfo = playlistService.findPlaylistsWithPaging(pageNum, pageSize);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.OK.value())
                .timestamp(timestamp)
                .message("Playlists have been found!")
                .data(playlistDtoPageInfo)
                .build();
    }

    @PostMapping
    Rest<?> createNewPlaylist(@Valid @RequestBody PlaylistRequest playlistRequest) {

        var playlistDto = playlistService.createNewPlaylist(playlistRequest);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.CREATED.value())
                .timestamp(timestamp)
                .message("Playlist has been created!")
                .data(playlistDto)
                .build();
    }

    @PutMapping("{uuid}")
    Rest<?> updatePlaylistByUuid(@PathVariable String uuid, @Valid @RequestBody PlaylistRequest playlistRequest) {

        var playlistDto = playlistService.updatePlaylistByUuid(uuid, playlistRequest);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.CREATED.value())
                .timestamp(timestamp)
                .message("Playlist has been updated!")
                .data(playlistDto)
                .build();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{uuid}")
    void deletePlaylistByUuid(@PathVariable String uuid) {
        playlistService.deletePlaylistByUuid(uuid);
    }
}
