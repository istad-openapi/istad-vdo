package co.istad.istadvdo.api.playlist;

import org.apache.ibatis.jdbc.SQL;

public class PlaylistProvider {

    public String buildSelectByUuidSql() {
        return new SQL() {{
            SELECT("*");
            FROM("playlists");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String buildInsertSql() {
        return new SQL() {{
            INSERT_INTO("playlists");
            VALUES("uuid", "#{playlist.uuid}");
            VALUES("title", "#{playlist.title}");
            VALUES("owner_id", "#{playlist.ownerId}");
            VALUES("visibility", "#{playlist.visibility}");
            VALUES("access_code", "#{playlist.accessCode}");
            VALUES("thumbnail", "#{playlist.thumbnail}");
            VALUES("created_at", "#{playlist.createdAt}");
            VALUES("updated_at", "#{playlist.updatedAt}");
            VALUES("status", "#{playlist.status}");
        }}.toString();
    }

    public String buildUpdatePlaylistByUuidSql() {
        return new SQL() {{
            UPDATE("playlists");
            SET("title = #{title}");
            SET("owner_id = #{ownerId}");
            SET("visibility = #{visibility}");
            SET("access_code = #{accessCode}");
            SET("thumbnail = #{thumbnail}");
            SET("updated_at = #{updatedAt}");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String buildSelectByIdSql() {
        return new SQL() {{
            SELECT("*");
            FROM("playlists");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String buildDeletePlaylistByUuidSql() {
        return new SQL() {{
            DELETE_FROM("playlists");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

}
