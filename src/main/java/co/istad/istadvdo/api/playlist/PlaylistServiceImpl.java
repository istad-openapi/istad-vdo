package co.istad.istadvdo.api.playlist;

import co.istad.istadvdo.api.playlist.web.PlaylistDto;
import co.istad.istadvdo.api.playlist.web.PlaylistRequest;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PlaylistServiceImpl implements PlaylistService {

    private final PlaylistMapper playlistMapper;
    private final PlaylistMapStruct playlistMapStruct;

    @Override
    public PlaylistDto findPlaylistByUuid(String uuid) {

        Playlist playlist = this.checkPlaylistByUuid(uuid);

        return playlistMapStruct.toPlaylistDto(playlist);
    }

    @Override
    public PageInfo<PlaylistDto> findPlaylistsWithPaging(int pageNum, int pageSize) {

        PageInfo<Playlist> playlistPageInfo = PageHelper
                .startPage(pageNum, pageSize)
                .doSelectPageInfo(playlistMapper::select);


        return playlistMapStruct.toPlaylistDtoPageInfo(playlistPageInfo);
    }

    @Override
    public PlaylistDto createNewPlaylist(PlaylistRequest playlistRequest) {

        Playlist playlist = playlistMapStruct.fromPlaylistRequest(playlistRequest);

        // Set up playlist for insert into db
        playlist.setUuid(UUID.randomUUID().toString());
        playlist.setAccessCode("ISTAD2023");
        playlist.setCreatedAt(LocalDateTime.now());
        playlist.setUpdatedAt(LocalDateTime.now());
        playlist.setStatus(true);

        playlistMapper.insert(playlist);

        playlist = playlistMapper.selectById(playlist.getId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Playlist is not existed!",
                        new Throwable("Something went wrong!"))
        );

        return playlistMapStruct.toPlaylistDto(playlist);
    }

    @Override
    public PlaylistDto updatePlaylistByUuid(String uuid, PlaylistRequest playlistRequest) {

        Playlist playlist = this.checkPlaylistByUuid(uuid);
        playlist.setUpdatedAt(LocalDateTime.now());

        playlistMapper.updatePlaylistByUuid(playlist);

        return playlistMapStruct.toPlaylistDto(playlist);
    }

    @Override
    public void deletePlaylistByUuid(String uuid) {
        this.checkPlaylistByUuid(uuid);
        playlistMapper.deletePlaylistByUuid(uuid);
    }

    // Util function
    private Playlist checkPlaylistByUuid(String uuid) {
        return playlistMapper.selectByUuid(uuid).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Playlist is not existed!",
                        new Throwable("Something went wrong!")));
    }
}
