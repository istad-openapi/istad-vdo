package co.istad.istadvdo.api.playlist;

import co.istad.istadvdo.api.playlist.web.PlaylistDto;
import co.istad.istadvdo.api.playlist.web.PlaylistRequest;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface PlaylistService {

    PlaylistDto findPlaylistByUuid(String uuid);

    PageInfo<PlaylistDto> findPlaylistsWithPaging(int pageNum, int pageSize);

    PlaylistDto createNewPlaylist(PlaylistRequest playlistRequest);

    PlaylistDto updatePlaylistByUuid(String uuid, PlaylistRequest playlistRequest);

    void deletePlaylistByUuid(String uuid);

}
