package co.istad.istadvdo.api.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Builder
@Getter
public class Rest<T> {
    private String message;
    private Boolean status;
    private Integer code;
    private Timestamp timestamp;
    private T data;
}
