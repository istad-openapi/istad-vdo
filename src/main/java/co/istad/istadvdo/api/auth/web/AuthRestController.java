package co.istad.istadvdo.api.auth.web;

import co.istad.istadvdo.api.auth.AuthServiceImpl;
import co.istad.istadvdo.api.base.Rest;
import jakarta.mail.MessagingException;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@RestController
@RequestMapping("api/v1/auth")
@Slf4j
@RequiredArgsConstructor
public class AuthRestController {

    // Inject authService
    private final AuthServiceImpl authService;

    @PostMapping("login")
    Rest<?> login(@Valid @RequestBody LoginDto loginDto) {

        String token = authService.login(loginDto);

        return Rest.builder()
                .status(true)
                .message("You have been logged in successfully!")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(token)
                .build();
    }

    @GetMapping("verify")
    Rest<?> verify(@RequestParam String email,
                   @RequestParam String token) {

        // Verify with email and token
        authService.verify(email, token);

        return Rest.builder()
                .status(true)
                .message("You have been verified successfully!")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(email)
                .build();
    }

    @PostMapping("send-mail-verification")
    Rest<?> sendMailVerification(@RequestBody EmailDto emailDto) throws MessagingException {

        authService.sendMailVerification(emailDto);

        return Rest.builder()
                .status(true)
                .message("Please check your email and verify!")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(emailDto.email())
                .build();
    }

    @PostMapping("register")
    Rest<?> register(@RequestBody RegisterDto registerDto) {

        var authDto = authService.register(registerDto);

        return Rest.builder()
                .status(true)
                .message("You have registered successfully")
                .code(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .data(authDto)
                .build();
    }

}
