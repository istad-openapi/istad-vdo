package co.istad.istadvdo.api.auth;

import co.istad.istadvdo.api.user.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Mapper
public interface AuthMapper {

    @UpdateProvider(type = AuthProvider.class, method = "buildUpdateStatusSql")
    void updateStatus(@Param("email") String email,
                      @Param("status") Boolean status);

    @UpdateProvider(type = AuthProvider.class, method = "buildUpdateTokenSql")
    void updateToken(@Param("email") String email,
                     @Param("token") String token);

    @SelectProvider(type = AuthProvider.class,
        method = "buildVerifySql")
    boolean verify(@Param("email") String email,
                   @Param("token") String token);

    @Update("UPDATE users SET verified_token = #{user.verifiedToken} " +
            "WHERE email = #{user.email}")
    void updateVerifiedToken(@Param("user") User user);

    @Select("SELECT * FROM users WHERE email = #{email}")
    @Result(property = "familyName", column = "family_name")
    @Result(property = "givenName", column = "given_name")
    Optional<User> selectByEmail(@Param("email") String email);

    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    @InsertProvider(type = AuthProvider.class, method = "buildRegisterSql")
    void register(@Param("user") User user);

    @InsertProvider(type = AuthProvider.class, method = "buildCreateSubscriberSql")
    void createSubscriber(@Param("userId") Integer userId);

}
