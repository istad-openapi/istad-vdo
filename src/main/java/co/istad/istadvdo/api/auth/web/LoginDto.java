package co.istad.istadvdo.api.auth.web;

import jakarta.validation.constraints.NotBlank;

public record LoginDto(@NotBlank String usernameOrEmail,
                       @NotBlank String password) {
}
