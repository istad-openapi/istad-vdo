package co.istad.istadvdo.api.auth;

import co.istad.istadvdo.api.auth.web.AuthDto;
import co.istad.istadvdo.api.auth.web.RegisterDto;
import co.istad.istadvdo.api.user.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthMapStruct {

    User fromRegisterDto(RegisterDto registerDto);

    AuthDto toAuthDto(User user);

}
