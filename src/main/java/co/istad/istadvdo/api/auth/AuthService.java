package co.istad.istadvdo.api.auth;

import co.istad.istadvdo.api.auth.web.AuthDto;
import co.istad.istadvdo.api.auth.web.EmailDto;
import co.istad.istadvdo.api.auth.web.LoginDto;
import co.istad.istadvdo.api.auth.web.RegisterDto;
import jakarta.mail.MessagingException;

public interface AuthService {

    String login(LoginDto loginDto);

    void verify(String email, String token);

    void sendMailVerification(EmailDto emailDto) throws MessagingException;

    AuthDto register(RegisterDto registerDto);

}
