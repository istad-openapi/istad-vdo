package co.istad.istadvdo.api.auth.web;

public record EmailDto(String email) {
}
