package co.istad.istadvdo.api.auth;

import org.apache.ibatis.jdbc.SQL;

public class AuthProvider {

    public String buildUpdateStatusSql() {
        return new SQL(){{
            UPDATE("users");
            SET("status = #{status}");
            WHERE("email = #{email}");
        }}.toString();
    }

    public String buildUpdateTokenSql() {
        return new SQL(){{
            UPDATE("users");
            SET("verified_token = #{token}");
            WHERE("email = #{email}");
        }}.toString();
    }

    public String buildVerifySql() {
        return new SQL() {{
            SELECT("EXISTS(SELECT * " +
                    "FROM users " +
                    "WHERE email = #{email} AND verified_token = #{token})");
        }}.toString();
    }

    public String buildCreateSubscriberSql() {
        return new SQL() {{
            INSERT_INTO("users_roles");
            VALUES("user_id", "#{userId}");
            VALUES("role_id", "3");
        }}.toString();
    }

    public String buildRegisterSql() {
        return new SQL() {{
            INSERT_INTO("users");
            VALUES("uuid", "#{user.uuid}");
            VALUES("email", "#{user.email}");
            VALUES("family_name", "#{user.familyName}");
            VALUES("given_name", "#{user.givenName}");
            VALUES("gender", "#{user.gender}");
            VALUES("password", "#{user.password}");
            VALUES("created_at", "NOW()");
            VALUES("status", "#{user.status}");
        }}.toString();
    }

}
