package co.istad.istadvdo.api.auth;

import co.istad.istadvdo.api.auth.web.AuthDto;
import co.istad.istadvdo.api.auth.web.EmailDto;
import co.istad.istadvdo.api.auth.web.LoginDto;
import co.istad.istadvdo.api.auth.web.RegisterDto;
import co.istad.istadvdo.api.mail.Mail;
import co.istad.istadvdo.api.mail.MailServiceImpl;
import co.istad.istadvdo.api.user.User;
import co.istad.istadvdo.config.security.CustomUserDetails;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {

    private final AuthMapper authMapper;
    private final AuthMapStruct authMapStruct;
    private final BCryptPasswordEncoder passwordEncoder;
    private final MailServiceImpl mailService;
    private final DaoAuthenticationProvider daoAuthenticationProvider;
    private final JwtEncoder jwtEncoder;

    @Override
    public String login(LoginDto loginDto) {

        // Create authorities statically
        List<SimpleGrantedAuthority> authorities = List.of(
                new SimpleGrantedAuthority("WRITE"),
                new SimpleGrantedAuthority("READ")
//                new SimpleGrantedAuthority("FULL_CONTROL")
        );

        // Authenticate user
        Authentication auth = new UsernamePasswordAuthenticationToken(
                loginDto.usernameOrEmail(),
                loginDto.password(),
                authorities);
        daoAuthenticationProvider.authenticate(auth);

        Instant now = Instant.now();
        String scope = auth.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(" "));

        log.info("Scope = {}", scope);

        JwtClaimsSet jwtClaimsSet = JwtClaimsSet.builder()
                .issuer("self")
                .issuedAt(now)
                .expiresAt(now.plus(1L, ChronoUnit.HOURS))
                .subject(auth.getName())
                .claim("scope", scope)
                .build();

        return jwtEncoder.encode(JwtEncoderParameters.from(jwtClaimsSet)).getTokenValue();
    }

    @Override
    public void verify(String email, String token) {

        // Check user by email and token
        if (authMapper.verify(email, token)) {
            authMapper.updateToken(email, null);
            authMapper.updateStatus(email, true);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "User with this email is not exist in the db..!",
                    new Throwable("Something went wrong!"));
        }
    }

    @Override
    public void sendMailVerification(EmailDto emailDto) throws MessagingException {

        // Verify your system or db has email or not
        User user = authMapper.selectByEmail(emailDto.email()).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "User with this email is not exist in the db..!",
                        new Throwable("Something went wrong!")));

        var random = new Random();
        String token = String.format("%6d", random.nextInt(999999));
        user.setVerifiedToken(token);

        // Insert code into db
        authMapper.updateVerifiedToken(user);

        Mail<User> mail = new Mail<>();
        mail.setReceiver(emailDto.email());
        mail.setSubject("Account Verification");
        mail.setTemplate("mail-verification");
        mail.setAdditionalData(user);
        mailService.sendMail(mail);
    }

    @Override
    public AuthDto register(RegisterDto registerDto) {

        User user = authMapStruct.fromRegisterDto(registerDto);
        user.setUuid(UUID.randomUUID().toString());
        user.setStatus(false);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        authMapper.register(user);

        // Assign role - SUBSCRIBER
        authMapper.createSubscriber(user.getId());

        return authMapStruct.toAuthDto(user);
    }

}
