package co.istad.istadvdo.api.mail;

import lombok.Data;

@Data
public class Mail<T> {

    private String receiver;
    private String subject;
    private String template;
    private T additionalData;

}
