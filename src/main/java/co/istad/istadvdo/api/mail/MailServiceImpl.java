package co.istad.istadvdo.api.mail;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    @Value("${spring.mail.username}")
    private String sender;

    @Value("${api.base-uri}")
    private String apiBaseUri;

    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;

    @Override
    public void sendMail(Mail<?> mail) throws MessagingException {

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message);

        // Create context object: set variable for use
        // in template engine (HTML)
        var context = new Context();
        context.setVariable("additionalData", mail.getAdditionalData());
        context.setVariable("apiBaseUri", apiBaseUri);

        String html = templateEngine.process(mail.getTemplate(), context);

        messageHelper.setTo(mail.getReceiver());
        messageHelper.setFrom(sender);
        messageHelper.setText(html, true);
        messageHelper.setSubject(mail.getSubject());

        javaMailSender.send(message);

    }
}
